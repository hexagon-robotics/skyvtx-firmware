/*
 * Copyright 2019 Hexagon Robotics
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    MKL03Z32xxx4_Project.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include <stdint.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL03Z4.h"
#include "fsl_gpio.h"
#include "fsl_lptmr.h"
#include "fsl_port.h"
#include "fsl_common.h"
#include "fsl_flash.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/
/*
 *
 *	7 SEGMENT LED DISPLAY
 *
		g   f   com  a   b
        D2  D3       D4  D5


		e   d   com  c   h
        A0  A1       A2  A3
*/
#define LED_DISPLAY_G_PORT PORTB //D2
#define LED_DISPLAY_G_GPIO GPIOB
#define LED_DISPLAY_G_PIN  11U
#define LED_DISPLAY_F_PORT PORTB //D3
#define LED_DISPLAY_F_GPIO GPIOB
#define LED_DISPLAY_F_PIN  4U
#define LED_DISPLAY_A_PORT PORTB //D4
#define LED_DISPLAY_A_GPIO GPIOB
#define LED_DISPLAY_A_PIN  7U
#define LED_DISPLAY_B_PORT PORTB //D5
#define LED_DISPLAY_B_GPIO GPIOB
#define LED_DISPLAY_B_PIN  6U
#define LED_DISPLAY_E_PORT PORTA //A0
#define LED_DISPLAY_E_GPIO GPIOA
#define LED_DISPLAY_E_PIN  12U
#define LED_DISPLAY_D_PORT PORTB //A1
#define LED_DISPLAY_D_GPIO GPIOB
#define LED_DISPLAY_D_PIN  3U
#define LED_DISPLAY_C_PORT PORTB //A2
#define LED_DISPLAY_C_GPIO GPIOB
#define LED_DISPLAY_C_PIN  2U
#define LED_DISPLAY_H_PORT PORTB //A3
#define LED_DISPLAY_H_GPIO GPIOB
#define LED_DISPLAY_H_PIN  13U

#define BOARD_SW_GPIO BOARD_SW1_GPIO
#define BOARD_SW_PORT BOARD_SW1_PORT
#define BOARD_SW_GPIO_PIN BOARD_SW1_GPIO_PIN
#define BOARD_SW_IRQ BOARD_SW1_IRQ
#define BOARD_SW_IRQ_HANDLER BOARD_SW1_IRQ_HANDLER
#define BOARD_SW_NAME BOARD_SW1_NAME

#define SW1_LPTMR_BASE LPTMR0
#define SW1_LPTMR_IRQn LPTMR0_IRQn
#define LPTMR_SW1_HANDLER LPTMR0_IRQHandler
/* Get source clock for LPTMR driver */
#define LPTMR_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_LpoClk)
/* Define LPTMR microseconds counts value */
#define LPTMR_USEC_COUNT 700000U

#define BUFFER_LEN 4
/*******************************************************************************
 * Prototypes
 ******************************************************************************/
/*!
 * @brief delay a while.
 */
void delay(void);
void error_trap(void);
void app_finalize(void);
/*******************************************************************************
 * Variables
 ******************************************************************************/
 /*! @brief Flash driver Structure */
 static flash_config_t s_flashDriver;
 /*! @brief Buffer for program */
 static uint32_t s_buffer[BUFFER_LEN];
 /*! @brief Buffer for readback */
 static uint32_t s_buffer_rbc[BUFFER_LEN];
/* Whether the SW button is pressed */
volatile bool isFalling = true;
/* Whether the SW button is released */
volatile bool isRising = false;
/* Channel variable */
int ch = 0;
/* Band variable */
int bd = 0;
/* Power variable */
int pw = 0;
/* Counter variable */
int counter = 1;

flash_security_state_t securityStatus = kFLASH_SecurityStateNotSecure; /* Return protection status */
status_t result;    /* Return code from each flash driver function */
uint32_t destAdrss; /* Address of the target location */
uint32_t i, failAddr, failDat;
uint32_t pflashBlockBase = 0;
uint32_t pflashTotalSize = 0;
uint32_t pflashSectorSize = 0;

/*******************************************************************************
 * Code
 ******************************************************************************/
void delay(void)
{
    volatile uint32_t i = 0;
    for (i = 0; i < 100000; ++i)
    {
        __asm("NOP"); /* delay */
    }
}

// SKYWAVE VTX is configured as SPI Mode //

/* FORMULA OF TUNING FREQUENCY
	To convert from tuning frequency to actual 20-bits register value on address 0x1,
	here is the example for reference
	Synthesizer counter default setting to 5865 MHz for 5.8GHz band.
	Relationship of RF frequency (FRF) to N (SYN_RF_N_REG) and A
	(SYN_RF_A_REG) counters is:
	FRF = 2*(N*64+A)*(Fosc/R)
	For example: for default FRF = 5865MHz, Fosc = 8MHz, R=400 for 20KHz
	PLL reference clock.
	5865MHz = 2*(N*64+A)*(8MHz/400) -> N = 2291, A = 1 -> register 0x01 = 0x47981
*/

/* PROGRAMING SEQUENCE
When tunging a frequency on SKYWAVE VTX, there are 3 registers need to
be adjusted step by step for each tuning.
To tune 5865MHz for example, first step is programming 0xF register as
0x00000 for reset then delay for 300ms for calibration. Second step is
programming 0x1 register as 0x47981 for tuning frequency then delay for
30ms. Last step is programming 0x3 register as 0x0FFD7 for adjusting tuning-
curve
*/

// Channels 1-8
const uint16_t channelMap[] = {
    0x7981,	0x758D,	0x7199,	0x6DA5,	0x69B1,	0x65BD, 0x6209,	0x5E15, // Band A
    0x5F9D, 0x6338, 0x6713, 0x6AAE, 0x6E89, 0x7224, 0x75BF, 0x799A, // Band B
    0x5A21, 0x562D, 0x5239, 0x4E85, 0x7D35, 0x8129, 0x851D, 0x8911, // Band E
    0x610C, 0x6500, 0x68B4, 0x6CA8, 0x709C, 0x7490, 0x7884, 0x7C38, // Band F
    0x510A, 0x5827, 0x5F84, 0x66A1, 0x6DBE, 0x751B, 0x7C38, 0x8395  // Band R
};

// Channels 1-8 in Mhz format
const uint16_t channelFreqMap[] = {
  5865, 5845, 5825, 5805, 5785, 5765, 5745, 5725, // Band A
  5733, 5752, 5771, 5790, 5809, 5828, 5847, 5866, // Band B
  5705, 5685, 5665, 5645, 5885, 5905, 5925, 5945, // Band E
  5740, 5760, 5780, 5800, 5820, 5840, 5860, 5880, // Band F
  5658, 5695, 5732, 5769, 5806, 5843, 5880, 5917  // Band R
};

const volatile uint32_t *base[8] = {
		LED_DISPLAY_G_GPIO,
		LED_DISPLAY_F_GPIO,
		LED_DISPLAY_A_GPIO,
		LED_DISPLAY_B_GPIO,
		LED_DISPLAY_E_GPIO,
		LED_DISPLAY_D_GPIO,
		LED_DISPLAY_C_GPIO,
		LED_DISPLAY_H_GPIO
};
const volatile uint32_t pin[8] = {
		LED_DISPLAY_G_PIN,
		LED_DISPLAY_F_PIN,
		LED_DISPLAY_A_PIN,
		LED_DISPLAY_B_PIN,
		LED_DISPLAY_E_PIN,
		LED_DISPLAY_D_PIN,
		LED_DISPLAY_C_PIN,
		LED_DISPLAY_H_PIN
};

/* g, f, a, b, e, d, c, h */
const int channel[8][8]={
      {0,0,0,1,0,0,1,0},      //1
      {1,0,1,1,1,1,0,0},      //2
      {1,0,1,1,0,1,1,0},      //3
      {1,1,0,1,0,0,1,0},      //4
      {1,1,1,0,0,1,1,0},      //5
      {1,1,1,0,1,1,1,0},      //6
      {0,0,1,1,0,0,1,0},      //7
      {1,1,1,1,1,1,1,0}       //8
};

/* g, f, a, b, e, d, c, h */
const int band[5][8]={
		{1,1,1,1,1,0,1,0},		//A
		{1,1,0,0,1,1,1,0},		//B
		{1,1,1,0,1,1,0,0},		//E
		{1,1,1,0,1,0,0,0},		//F
		{0,1,1,0,1,0,0,0}		//R
};

/* g, f, a, b, e, d, c, h */
const int power[6][8]={
      {0,0,0,1,0,0,1,1},      //1.
      {1,0,1,1,1,1,0,1},      //2.
      {1,0,1,1,0,1,1,1},      //3.
      {1,1,0,1,0,0,1,1},      //4.
      {1,1,1,0,0,1,1,1},      //5.
      {1,1,1,0,1,1,1,1}       //6.
};

/*
* @brief Gets called when an error occurs.
*
* @details Print error message and trap forever.
*/
void error_trap(void)
{
    printf("\r\n\r\n\r\n\t---- HALTED DUE TO FLASH ERROR! ----");
    while (1)
    {
    }
}
/*!
 * @brief Interrupt service function of switch.
 *
 * This function change the channels.
 */
void BOARD_SW_IRQ_HANDLER(void)
{
	if(isFalling){
		isFalling = false;
		isRising = true;
		printf("is Falling \r\n");
	    /* 	Start counting */
		LPTMR_StopTimer(SW1_LPTMR_BASE);
		LPTMR_StartTimer(SW1_LPTMR_BASE);
	} else if(isRising){
		LPTMR_StopTimer(SW1_LPTMR_BASE);
		isFalling = true;
		isRising = false;
		printf("is Rising \r\n");

		if(counter==1) {
					 /* Turn off all leds */
					for(int i = 0; i<8;i++)	{GPIO_PortSet((void*)base[i], 1U << pin[i]);}
						/* Show the channel in the Led Display. */
					for(int i = 0; i<8;i++) {GPIO_PortToggle((void*)base[i], channel[ch][i] << pin[i]);}
						/* Reset state of button. */
					//isFalling = false;
						/* Reset the channel variable */
					if (ch >= 7) {
						ch = 0;
					} else {
						/* Increment the channel */
						ch++;
					}

				} else if (counter==2) {
					/* Turn off all leds */
					for(int i = 0; i<8;i++)	{GPIO_PortSet((void*)base[i], 1U << pin[i]);}
						/* Show the band in the Led Display. */
					for(int i = 0; i<8;i++) {GPIO_PortToggle((void*)base[i], band[bd][i] << pin[i]);}
						/* Reset state of button. */
					//isFalling = false;
						/* Reset the band variable */
					if (bd >= 4) {
						bd = 0;
					} else {
						/* Increment the band */
						bd++;
					}

				} else if (counter==3) {
					/* Turn off all leds */
					for(int i = 0; i<8;i++)	{GPIO_PortSet((void*)base[i], 1U << pin[i]);}
						/* Show the power in the Led Display. */
					for(int i = 0; i<8;i++) {GPIO_PortToggle((void*)base[i], power[pw][i] << pin[i]);}
						/* Reset state of button. */
					//isFalling = false;
						/* Reset the power */
					if (pw >= 5) {
						pw = 0;
					} else {
						/* Increment the channel */
						pw++;
					}
				}
		delay();
	}
	/*Clear external interrupt flag.*/
	GPIO_PortClearInterruptFlags(BOARD_SW_GPIO, 1U << BOARD_SW_GPIO_PIN);
    /*
 	 Add for ARM errata 838869, affects Cortex-M4, Cortex-M4F Store immediate overlapping
  	  exception return operation might vector to incorrect interrupt */
#if defined __CORTEX_M && (__CORTEX_M == 4U)
    __DSB();
#endif
}

/*!
 * @brief Input Capture
 *
 * This function change the bands.
 */
void LPTMR_SW1_HANDLER(void)
{
    LPTMR_ClearStatusFlags(SW1_LPTMR_BASE, kLPTMR_TimerCompareFlag);
    LPTMR_StopTimer(SW1_LPTMR_BASE);

    if (counter == 1) {
    	/* Turn off all leds */
		for(int i = 0; i<8;i++)	{GPIO_PortSet((void*)base[i], 1U << pin[i]);}
			/* Show the band in the Led Display. */
		for(int i = 0; i<8;i++) {GPIO_PortToggle((void*)base[i], band[bd][i] << pin[i]);}
    	printf("Band Mode activated \r\n");
    	counter++;
	} else if (counter == 2) {
		/* Turn off all leds */
		for(int i = 0; i<8;i++)	{GPIO_PortSet((void*)base[i], 1U << pin[i]);}
			/* Show the power in the Led Display. */
		for(int i = 0; i<8;i++) {GPIO_PortToggle((void*)base[i], power[pw][i] << pin[i]);}
		printf("Power Mode activated \r\n");
		counter++;
	} else if (counter == 3){
		 /* Turn off all leds */
		for(int i = 0; i<8;i++)	{GPIO_PortSet((void*)base[i], 1U << pin[i]);}
			/* Show the channel in the Led Display. */
		for(int i = 0; i<8;i++) {GPIO_PortToggle((void*)base[i], channel[ch][i] << pin[i]);}
		printf("Channel Mode activated \r\n");
		counter = 1;
	}
    /*
     * Workaround for TWR-KV58: because write buffer is enabled, adding
     * memory barrier instructions to make sure clearing interrupt flag completed
     * before go out ISR
     */
    __DSB();
    __ISB();
}
/*!
 * @brief Erase Flash Sector
 *
 * Erase several sectors on upper pflash block where there is no code
 */
void eraseSector(void){

	        printf("\r\n Erase a sector of flash");

	/* In case of the protected sectors at the end of the pFlash just select
	the block from the end of pFlash to be used for operations
	SECTOR_INDEX_FROM_END = 1 means the last sector,
	SECTOR_INDEX_FROM_END = 2 means (the last sector - 1) ...
	in case of FSL_FEATURE_FLASH_HAS_PFLASH_BLOCK_SWAP it is
	SECTOR_INDEX_FROM_END = 1 means the last 2 sectors with width of 2 sectors,
	SECTOR_INDEX_FROM_END = 2 means the last 4 sectors back
	with width of 2 sectors ...
	*/
	#ifndef SECTOR_INDEX_FROM_END
	  #define SECTOR_INDEX_FROM_END 1U
	#endif

	/* Erase a sector from destAdrss. */
	#if defined(FSL_FEATURE_FLASH_HAS_PFLASH_BLOCK_SWAP) && FSL_FEATURE_FLASH_HAS_PFLASH_BLOCK_SWAP
	        /* Note: we should make sure that the sector shouldn't be swap indicator sector*/
	        destAdrss = pflashBlockBase + (pflashTotalSize - (SECTOR_INDEX_FROM_END * pflashSectorSize * 2));
	#else
	        destAdrss = pflashBlockBase + (pflashTotalSize - (SECTOR_INDEX_FROM_END * pflashSectorSize));
	#endif

	        result = FLASH_Erase(&s_flashDriver, destAdrss, pflashSectorSize, kFLASH_ApiEraseKey);
	        if (kStatus_FLASH_Success != result)
	        {
	            error_trap();
	        }

	        /* Verify sector if it's been erased. */
	        result = FLASH_VerifyErase(&s_flashDriver, destAdrss, pflashSectorSize, kFLASH_MarginValueUser);
	        if (kStatus_FLASH_Success != result)
	        {
	            error_trap();
	        }

	        /* Print message for user. */
	        printf("\r\n Successfully Erased Sector 0x%x -> 0x%x\r\n", destAdrss, (destAdrss + pflashSectorSize));
}


void programFlash(void){
	 printf("\r\n Program a buffer to a sector of flash ");
	        /* Prepare user buffer. */
	        for (i = 0; i < BUFFER_LEN; i++)
	        {
	            s_buffer[i] = i;
	        }
	        /* Program user buffer into flash*/
	        result = FLASH_Program(&s_flashDriver, destAdrss, s_buffer, sizeof(s_buffer));
	        if (kStatus_FLASH_Success != result)
	        {
	            error_trap();
	        }
}

void readFlash(void){
#if defined(__DCACHE_PRESENT) && __DCACHE_PRESENT
        /* Clean the D-Cache before reading the flash data*/
        SCB_CleanInvalidateDCache();
#endif
        /* Verify programming by reading back from flash directly*/
        for (uint32_t i = 0; i < BUFFER_LEN; i++)
        {
            s_buffer_rbc[i] = *(volatile uint32_t *)(destAdrss + i * 4);
            if (s_buffer_rbc[i] != s_buffer[i])
            {
                error_trap();
            }
        }

        printf("\r\n Successfully Programmed and Verified Location 0x%x -> 0x%x \r\n", destAdrss,
               (destAdrss + sizeof(s_buffer)));

        /* Erase the context we have progeammed before*/
        /* Note: we should make sure that the sector which will be set as swap indicator should be blank*/
        FLASH_Erase(&s_flashDriver, destAdrss, pflashSectorSize, kFLASH_ApiEraseKey);
}

/*
 * @brief   Application entry point.
 */
int main(void) {
	lptmr_config_t lptmrConfig;

	 /* Define the init structure for the input switch pin */
	    gpio_pin_config_t sw_config = {
	        kGPIO_DigitalInput, 0,
	    };

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    /* Clean up Flash driver Structure*/
       memset(&s_flashDriver, 0, sizeof(flash_config_t));

       /* Setup flash driver structure for device and initialize variables. */
       result = FLASH_Init(&s_flashDriver);
       if (kStatus_FLASH_Success != result)
       {
           error_trap();
       }
       /* Get flash properties*/
       FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflashBlockBaseAddr, &pflashBlockBase);
       FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflashTotalSize, &pflashTotalSize);
       FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflashSectorSize, &pflashSectorSize);

       /* Print flash information - PFlash. */
       printf("\r\n PFlash Information: ");
       printf("\r\n Total Program Flash Size:\t%d KB, Hex: (0x%x)", (pflashTotalSize / 1024), pflashTotalSize);
       printf("\r\n Program Flash Sector Size:\t%d KB, Hex: (0x%x) ", (pflashSectorSize / 1024), pflashSectorSize);

       /* Check security status. */
          result = FLASH_GetSecurityState(&s_flashDriver, &securityStatus);
          if (kStatus_FLASH_Success != result)
          {
              error_trap();
          }
          /* Print security status. */
          switch (securityStatus)
          {
              case kFLASH_SecurityStateNotSecure:
                  printf("\r\n Flash is UNSECURE!");
                  break;
              case kFLASH_SecurityStateBackdoorEnabled:
                  printf("\r\n Flash is SECURE, BACKDOOR is ENABLED!");
                  break;
              case kFLASH_SecurityStateBackdoorDisabled:
                  printf("\r\n Flash is SECURE, BACKDOOR is DISABLED!");
                  break;
              default:
                  break;
          }
          printf("\r\n");

    /* Configure LPTMR */
	/*
	 * lptmrConfig.timerMode = kLPTMR_TimerModeTimeCounter;
	 * lptmrConfig.pinSelect = kLPTMR_PinSelectInput_0;
	 * lptmrConfig.pinPolarity = kLPTMR_PinPolarityActiveHigh;
	 * lptmrConfig.enableFreeRunning = false;
	 * lptmrConfig.bypassPrescaler = true;
	 * lptmrConfig.prescalerClockSource = kLPTMR_PrescalerClock_1;
	 * lptmrConfig.value = kLPTMR_Prescale_Glitch_0;
	 */
	LPTMR_GetDefaultConfig(&lptmrConfig);

	/* Initialize the LPTMR */
	LPTMR_Init(SW1_LPTMR_BASE, &lptmrConfig);

    /*
     * Set timer period.
     * Note : the parameter "ticks" of LPTMR_SetTimerPeriod should be equal or greater than 1.
    */
    LPTMR_SetTimerPeriod(SW1_LPTMR_BASE, USEC_TO_COUNT(LPTMR_USEC_COUNT, LPTMR_SOURCE_CLOCK));

    /* Enable timer interrupt */
    LPTMR_EnableInterrupts(SW1_LPTMR_BASE, kLPTMR_TimerInterruptEnable);

    /* Enable at the NVIC */
    EnableIRQ(SW1_LPTMR_IRQn);

    /* Init input switch GPIO. */
    PORT_SetPinInterruptConfig(BOARD_SW_PORT, BOARD_SW_GPIO_PIN, kPORT_InterruptEitherEdge);
    EnableIRQ(BOARD_SW_IRQ);
    GPIO_PinInit(BOARD_SW_GPIO, BOARD_SW_GPIO_PIN, &sw_config);

	/* Show the initial channel */
	for(int i = 0; i<8;i++){GPIO_PortToggle((void*)base[i], channel[0][i] << pin[i]);}
	ch++;

	while(1){
		if(isRising){

		}
	}
}
